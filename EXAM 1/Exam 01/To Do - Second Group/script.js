let totalTasks = 0;
let doneTasks = 0;
let leftTasks = 0;

function addTask() {
let taskInput = document.getElementById("taskInput");
let taskText = taskInput.value.trim();

if (taskText !== "") {
totalTasks++;
leftTasks++;
let taskList = document.getElementById("taskList");
let newTask = document.createElement("li");
newTask.innerHTML = `<input type="checkbox" onchange="toggleTask(${taskList.children.length - 1})"> <span id="task${taskList.children.length - 1}">${taskText}</span>`;
taskList.appendChild(newTask);
document.getElementById(`task${taskList.children.length - 1}`).onclick = function () {
if (this.style.textDecoration == "line-through") {
this.style.textDecoration = "none";       
leftTasks++;
doneTasks--;
} else {
this.style.textDecoration = "line-through";
leftTasks--;
doneTasks++;
}
updateTaskCounts();
};
updateTaskCounts();
taskInput.value = "";
}
}

function toggleTask(index) {
let taskElement = document.getElementById(`task${index}`);
if (taskElement.style.textDecoration == "line-through") {
taskElement.style.textDecoration = "none";
leftTasks++;
doneTasks--;
} else {
taskElement.style.textDecoration = "line-through";
leftTasks--;
doneTasks++;
}
updateTaskCounts();
}

function updateTaskCounts() {
    document.getElementById("totalTasks").innerText = totalTasks;
    document.getElementById("doneTasks").innerText = doneTasks;
    document.getElementById("leftTasks").innerText = leftTasks;
}