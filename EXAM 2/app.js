const addButton = document.getElementById("add-btn");
let booksList = document.getElementById("books-list");
const shoppingCart = document.getElementById("shopping-cart");
const wishlist = document.getElementById("wishlist");

addButton.addEventListener("click", function(e) {
    e.preventDefault();
    const title = document.getElementById("title").value;
    const author = document.getElementById("author").value;
    const thumbnail = document.getElementById("thumbnail").value;
    let price = document.getElementById("price").value;
    let q = document.getElementById("quantity").value;
    const description = document.getElementById("description").value;

    const book = document.createElement("li");
    book.setAttribute("class", "card");
    book.innerHTML = `<div class="card-title">${title} - ${author}</div> <div class="card-img"><img src="${thumbnail}"></div> <div class="card-description">${description}</div> <div class="card-price">Price: ${price}</div> <div class="card-quantity">Quantity: ${q}</div>
        <div class="card-actions"> <button class="btn-buy">Buy</button> <button class="wishlist-btn">Add to Wishlist</button></div>`;
    booksList.appendChild(book);

    const ButtonBuy = book.querySelector(".btn-buy");
    const wishlistButton = book.querySelector(".wishlist-btn");

    ButtonBuy.addEventListener("click", function() {
        if (parseInt(q) > 0) {
            const shoppingCartItem = document.createElement("li");
            shoppingCartItem.textContent = `${title} - ${author} ${price}`;
            shoppingCart.appendChild(shoppingCartItem);
            q--;
            book.querySelector(".card-quantity").textContent = `Quantity: ${q}`;
            if (q === 0) {
            ButtonBuy.style.display = "none";
            wishlistButton.style.display = "none";
            }
            shoppingCartItem.innerHTML += ` <a href="#" class="remove-book-cart">Remove</a>`;
            shoppingCartItem.querySelector(".remove-book-cart").addEventListener("click", function(e) {
            e.preventDefault();
            shoppingCartItem.remove();
            buyButton.style.display = "block";
            wishlistButton.style.display = "block";
            q++;
            book.querySelector(".card-quantity").textContent = `Quantity: ${q}`;
            });
        }
    });
    wishlistButton.addEventListener("click", function() {
        const wishlistItem = document.createElement("li");
        wishlistItem.innerHTML = `
        <div class="card-title">${title} - ${author}</div>
        <div class="card-price">Price: ${price}</div>
        <div class="card-actions">
        <a href="#" class="remove-book-cart">Remove</a>
        </div>
        `;
        wishlist.appendChild(wishlistItem);
        q--;
        book.querySelector(".card-quantity").textContent = `Quantity: ${q}`;
        wishlistItem.querySelector(".remove-book-cart").addEventListener("click", function(e) {
        e.preventDefault();
        wishlistItem.remove();
        });
    });
});
