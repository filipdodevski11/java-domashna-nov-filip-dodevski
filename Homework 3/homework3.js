function MusicAlbum(Name,Artist,ArtistImage,Songs,yearReleased,albumCover,albumDuration) {
  this.name = Name;
  this.artist = Artist;
  this.artistImage = ArtistImage;
  this.songs = Songs;
  this.yearReleased = yearReleased;
  this.albumCover = albumCover;
  this.albumDuration = albumDuration;

  this.addSong = function (song) {
    this.songs.push(song);
  };

  this.albumHeader = function () {
    const albumHeaderDiv = document.querySelector(".album-header");
    let html = "";
    html += `<div class="image">
                ${this.albumCover}
             </div>
             <div> 
                <h3>Album</h3>
                <h1>${this.name}</h1>
                <div class="artistInfo">
                    <div>${this.artistImage}</div>
                    <div>${this.artist}</div>
                    <div>${this.yearReleased}</div>
                    <div>${this.songs.length}</div>
                    <div>${this.albumDuration}</div>
                </div>
                
             </div>
             
             
             `;
    albumHeaderDiv.innerHTML += html;
  };

  this.listAllSongs = function () {
    const albumContainer = document.querySelector(".album-container");
    let html = "";
    html += `<ol>`;
    for (let song of this.songs) {
      html += `
            <li>
                <div>
                    <div>
                        ${song.title}<br>
                        ${song.artist}<br>
                    </div>
                    <div>
                        ${song.plays}<br>

                    </div>
                    <div>
                        ${song.duration}
                    </div>
                </div>
            </li>

        `;
    }
    html += `</ol>`;
    albumContainer.innerHTML += html;
  };
}


const theAlbum = new MusicAlbum("Rosalia Mix Made for you", "Rosalia", "<img src='img/rosa1.png 'width=100px>", [], "July, 2020", "<img src='img/rosa2.jpeg' width=300px>", "2hr 28 min");

function Song(title, artist, plays, duration) {
  this.title = title;
  this.artist = artist;
  this.plays = plays;
  this.duration = duration;
}

const song1 = new Song("El Panuelo", "Rosalia & Romeo Santos", "33,333,333", "3:55");
const song2 = new Song("Yo x Ti, Tu x Mi", "Rosalia & Ozuna", "33,333,333", "3:21");
const song3 = new Song("La NOCHE DE ANOCHE", "Bad Bunny & Rosalia", "33,333,333", "3:23");
const song4 = new Song("DESPECHA", "Rosalia", "33,333,333", "2:37");
const song5 = new Song("La FAMA", "Rosalia & The Weeknd", "33,333,333", "3:08");
const song6 = new Song("LLYLM", "Rosalia", "33,333,333", "2:54");

theAlbum.addSong(song1);
theAlbum.addSong(song2);
theAlbum.addSong(song3);
theAlbum.addSong(song4);
theAlbum.addSong(song5);
theAlbum.addSong(song6);

theAlbum.listAllSongs();
theAlbum.albumHeader();
