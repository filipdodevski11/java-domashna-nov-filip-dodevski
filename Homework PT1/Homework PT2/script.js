// Step 1: Create an array with numbers
let numbers = [10, 1, 13];
// Step 2: Create an unordered list element in the HTML body
let list = document.createElement('ul');
document.body.appendChild(list);
// Step 3: Iterate through the array and create a list item for each number
let sum = 0;
numbers.forEach(number => {
let listItem = document.createElement('li');
listItem.textContent = number;
list.appendChild(listItem);
sum += number;
});

// Step 4: Display the sum and the mathematical equation
let sumParagraph = document.createElement('p');
sumParagraph.textContent = `Suma: ${sum}`;
document.body.appendChild(sumParagraph);

let equationParagraph = document.createElement('p');
equationParagraph.textContent = `Presmetka: ${numbers.join(' + ')} = ${sum}`;
document.body.appendChild(equationParagraph);
