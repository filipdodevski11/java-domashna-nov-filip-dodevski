// Change the text of the h1 with id 'myTitle'
document.getElementById("myTitle").innerText = "New Title!";

// Change the text of the p with class 'paragraph'
document.getElementsByClassName("paragraph")[0].innerText = "This is a new exercise.";
// Change the text of the p with class 'paragraph second'
document.getElementsByClassName("paragraph second")[0].innerText = "Yes, it's really eZ!";

//Second header
let secondHeader = document.getElementsByTagName("h1")[1];
console.log(secondHeader);
secondHeader.innerText ="This is the second header"

//The last header 
let lastHeader = document.getElementsByTagName("h3")[0];
console.log(lastHeader);
lastHeader.innerText += " I will not change this text, i will just add some new text"