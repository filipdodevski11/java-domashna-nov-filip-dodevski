//pt1
const btn1 = document.querySelector(".submit");
const table = document.querySelector("table");

btn1.addEventListener("click", function () {
  let firstName = document.getElementById("firstName").value;
  let lastName = document.getElementById("lastName").value;
  let email = document.getElementById("email").value;
  let password = document.getElementById("password").value;

  let addTR = document.createElement("tr");
  table.appendChild(addTR);

  let firstName1 = document.createElement("td");
  firstName1.innerText = firstName;
  addTR.appendChild(firstName1);

  let lastName2 = document.createElement("td");
  lastName2.innerText = lastName;
  addTR.appendChild(lastName2);

  let email3 = document.createElement("td");
  email3.innerText = email;
  addTR.appendChild(email3);

  let password4 = document.createElement("td");
  password4.innerText = password;
  addTR.appendChild(password4);
});

//pt2

const btn2 = document.querySelector(".create");
const Table2 = document.querySelector(".forTable");

btn2.addEventListener("click", function () {
  const numOfColumns = document.querySelector("#columns").value;
  const numOfRows = document.querySelector("#rows").value;

  for (i = 1; i <= numOfColumns; i++) {
    let createColumn = document.createElement("tr");
    Table2.appendChild(createColumn);
    for (j = 1; j <= numOfRows; j++) {
      let createRow = document.createElement("td");
      createRow.textContent = `Column${j} Row${j}`;
      createColumn.appendChild(createRow);
    }
    Table2.appendChild(createColumn);
  }
});
